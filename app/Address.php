<?php

namespace App;

use App\Http\Controllers\Driver\BFC;
use App\Http\Controllers\Drivers\Bitcoin;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //

   static function singalton($user_id,$account_id,$identifier,$currency='bfc'){
            $address = Address::where('account_id',$account_id);
            if($address->count()){
                $address = $address->first();
            }else{

                  $waddress =   Bitcoin::create($identifier);




                $address = new Address();
                $address->address= $waddress;
                $address->private_key= "";

                $address->currency = $currency;
                $address->account_id = $account_id;
                $address->user_id = $user_id;

                $address->save();
            }
            return $address;

    }


    function user(){
        return $this->belongsTo('App\User');
    }
    function account(){
        return $this->belongsTo('App\Account');
    }
}
