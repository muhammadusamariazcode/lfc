<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','parent_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function type(){
        if($this->is_admin){
            return "Admin";
        }else{
            return "User";
        }
    }

    function selfi(){
        return "/vendor/tabler/demo/faces/female/25.jpg";
    }


    function google2fa_secret_url(){
        $google2fa = new Google2FA();
        if(empty($this->google2fa_secret)){
            $secret  = $google2fa->generateSecretKey();
            // $google2fa_url ="otpauth://totp/coinmama:$user->email?secret=$secret&issuer=CoinMama";
            $company = config('app.name','');

            $company = str_replace(" ","%20",$company);
            $email = $this->email;
            $this->google2fa_secret =$secret ;

            $google2fa_url ="otpauth://totp/$company:$email?secret=$secret&issuer=$company";

            $this->google2fa_secret_url =$google2fa_url ;
            $this->google2fa_company= $company;
            $this->save();

        }

        return  $this->google2fa_secret_url;
    }

    function google2fa_inline(){
        $google2fa = new Google2FA();

        $this->google2fa_secret_url();
        $inlineUrl = $google2fa->getQRCodeInline(
            $this->google2fa_company,
            $this->email,
            $this->google2fa_secret
        );
        return $inlineUrl;
    }


    static function security(){
        $user = \Auth::user();
        if($user->is_google2fa){
            $is_google_2fa_session =  session('is_google_2fa_session',false);
            if($is_google_2fa_session){
                return null;
            }

            return redirect()->route('auth.google2fa.ask');
        }
        return null;

    }



    static function is_exit($id){
        return User::where('id',$id)->count() !=0;
    }
    function make_referral_entries(){
        ///for level one
        $user = $this;

        for($level = 1;$level<=20;$level++){
            $parent_id = $user->parent_id;
            if(User::is_exit($parent_id)){
                $parent = User::find($parent_id);
                Referral::singleton($this->id,$parent_id,$level);

                $user = $parent;

            }else{
                break;
            }
        }

    }



    function count_referrals($level = 0){
        if($level <0 || $level > 20){
            return 0;
        }
        if($level == 0){
            return Referral::where('parent_id',$this->id)->count();
        }
        return Referral::where('parent_id',$this->id)->where('level',$level)->count();


    }
    function balance($currency ='bfc'){

        //return $this->balance;
        $account = $this->account($currency);
        return $account->balance;
    }
    function default_address($currency ='bfc'){
       return $this->account($currency)->default_address();
    }
    function is_active(){

        return $this->is_active;


    }
    function update_activation(){
        $is_active = 0;
        if($this->balance() >= env('MIN_BALNCE_TO_BE_ACTIVE',1000)){
            $is_active = 1;
        }

        $this->is_active = $is_active;
        $this->save();

        Referral::where('parent_id',$this->id)->update(['is_active'=>$is_active]);
    }


    function status($level){
        if($level == 0){
            if($this->is_active()){
                return 'Active';
            }
        }
        if($level == 1){
            if($this->is_active() && Referral::countActiveReferral($this->id,1) >=5){
                return 'Active';
            }

        }
        if($level == 2){
            if($this->status($level -1) =='Active' && $this->balance() >= 2000 && Referral::countActiveReferral($this->id,2) >=15){
                return 'Active';
            }

        }
        if($level == 3){
            if($this->status($level -1) =='Active' && $this->balance() >= 3000 && Referral::countActiveReferral($this->id,3) >=1 ){
                return 'Active';
            }
        }
        if($level == 4){
            if($this->status($level -1) =='Active'  && $this->balance() >= 4000 && Referral::countActiveReferral($this->id,4) >=1){
                return 'Active';
            }

        }
        if($level == 5){
            if($this->status($level -1) =='Active'   && $this->balance() >= 5000 && Referral::countActiveReferral($this->id,5) >=1){
                return 'Active';
            }

        }
        if($level == 6){
            if($this->status($level -1) =='Active'   && $this->balance() >= 6000 && Referral::countActiveReferral($this->id,6) >=1){
                return 'Active';
            }

        }
        if($level == 7){
            if($this->status($level -1) =='Active'   && $this->balance() >= 7000 && Referral::countActiveReferral($this->id,7) >=1){
                return 'Active';
            }

        }
        if($level == 8){
            if($this->status($level -1) =='Active'   && $this->balance() >= 8000 && Referral::countActiveReferral($this->id,8) >=1){
                return 'Active';
            }

        }
        if($level == 9){
            if($this->status($level -1) =='Active'   && $this->balance() >= 9000 && Referral::countActiveReferral($this->id,9) >=1){
                return 'Active';
            }

        }
        if($level == 10){
            if($this->status($level -1) =='Active'   && $this->balance() >= 10000 && Referral::countActiveReferral($this->id,10) >=1){
                return 'Active';
            }

        }
        if($level ==11){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,11) >=1){
                return 'Active';
            }

        }
        if($level == 12){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,12) >=1){
                return 'Active';
            }

        }
        if($level == 13){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,13) >=1){
                return 'Active';
            }

        }
        if($level == 14){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,14) >=1){
                return 'Active';
            }

        }
        if($level == 15){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,15) >=1){
                return 'Active';
            }

        }
        if($level == 16){
            if($this->status($level -1) =='Active'  && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,16) >=1){
                return 'Active';
            }

        }
        if($level == 17){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,17) >=1){
                return 'Active';
            }

        }
        if($level == 18){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,18) >=1){
                return 'Active';
            }

        }
        if($level == 19){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,19) >=1){
                return 'Active';
            }

        }
        if($level == 20){
            if($this->status($level -1) =='Active'   && $this->balance() >= 20000 && Referral::countActiveReferral($this->id,20) >=1){
                return 'Active';
            }

        }
        return 'De-Active';
    }


    function parent(){
        return $this->belongsTo('App\User');
    }
    function parentByLevel($level){
        if($level <1 || $level >20){
            return null;
        }
        $parent_id  = $this->parent_id;
        $curr = $this;
        while($level != 0){
            if(User::is_exit($parent_id)){
                $curr = $curr->parent;
            }else{
                return null;
            }

            $level--;
        }
        return $curr;

    }


    function account($currency = 'bfc'){
        return Account::singalton($this->id,$currency);
    }

    static function ApiUser(\Illuminate\Http\Request $request){
        return  User::findOrFail($request->user_id);
    }

    static function ApiLogin($email,$password){
        $validator =    Validator::make(['email'=>$email,'password'=>$password], [

            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        if($validator->fails()){
            throw new \Exception($validator->errors()->first());
        }

        $user = User::select(['id','name','email','password','auth_token','total_stake_earned','total_referral_commission'])->where('email',$email);
        if($user->count()==0){
            throw new \Exception("INVALID_CREDENTIALS");
        }

        $user = $user->first();
        if (!Hash::check($password, $user->password)) {
            throw new \Exception("INVALID_CREDENTIALS");
        }
        $user->updateAuthToken();

        return $user;



    }

    static function ApiRegister($email,$name,$password){
        $validator =    Validator::make(['email'=>$email,'password'=>$password,'name'=>$name], [

            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if($validator->fails()){
            throw new \Exception($validator->errors()->first());
        }

        $user =  User::create([

            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
            'parent_id'=>1

        ]);
        $user->make_referral_entries();
        $user->updateAuthToken();

        return $user;




    }
    static function ApiBalance(User $user){
        $r['current_balance'] = $user->balance();
        $r['total_stake_earned'] = $user->total_stake_earned;
        $r['total_referral_commission'] = $user->total_referral_commission;
        return $r;
    }
    static function ApiUserR(User $user){
        $r['user_id'] = $user->id;
        $r['auth_token'] = $user->auth_token;
        $r['name'] = $user->name;
        $r['email'] = $user->email;

        $r['address'] = $user->default_address()->address;
        return $r;
    }


    function updateAuthToken(){
        $this->auth_token = md5($this->id)."_".self::generateRandomString2(50);
        $this->save();
    }

    static  public function generateRandomString2($length = 100)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}
