<?php

namespace App;

use App\Http\Controllers\Driver\BFC;
use  App\Http\Controllers\Drivers\Bitcoin;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //

    static function round($number){
        return number_format((float)$number, 8, '.', '');
    }

   static function singalton($user_id,$currency='bfc',$title='DEFAULT'){
        $account = Account::where('user_id',$user_id)->where('currency',$currency)->where('title',$title);
        if($account->count()){
            $account = $account->first();


        }else{
            $account = new Account();
            $account->user_id = $user_id;
            $account->currency = $currency;
            $account->title = $title;

            $account->save();
            $account->identifier = $user_id . "_". $account->id . "_".$currency;

            $account->save();



        }

       return $account;
    }
    function default_address(){

        return Address::singalton($this->user_id,$this->id,$this->identifier,$this->currency);
    }
    function send($to,$amount,$purpose="",$is_sync=1){
        if($this->currency=='bfc'){
            $txid = Bitcoin::sendfrom($this->identifier,$to,$amount);
            if($is_sync){
                $this->sync_balance();

            }


            return $txid;
        }


    }


    function transacions($count=10,$from=0){
        $transactions =  Bitcoin::listtransactions($this->identifier,$count,$from);


        ($transactions);
        $db =[];
        foreach($transactions as $transaction){
            $t['hash'] = @$transaction['txid'];
            $t['currency'] = "BFC";
            $t['amount'] = @$transaction['amount'];
            $t['address'] = @$transaction['address'];
            $t['type'] = @$transaction['category'];
            $t['confirmations'] = @$transaction['confirmations'];
            $t['time'] = @$transaction['time'];
            $t['fee'] = @$transaction['fee'];
            $t['is_stake'] = 0;
            $t['is_commission'] = 0;

            $t['purpose'] = "";
            if(Stake::where('tx_hash',$t['hash'])->count()){
                $t['purpose'] = 'Staking';
                $t['is_stake'] = 1;
            }

            if(Commission::where('tx_hash',$t['hash'])->count()){
                $t['purpose'] = 'Referral Commission';
                $t['is_commission'] = 1;

            }

       $db[] = $t;



        }
        return $db;
    }



    function user(){
        return $this->belongsTo('App\User');
    }


    function sync(){

        $this->default_address();
        sleep(1);
        $this->sync_transactions();
        sleep(1);

        $this->sync_balance();
        sleep(1);

        $this->sync_confirmations();
        sleep(1);


    }

    function sync_transactions(){
        $transactions = $this->transacions(30,0);
        //  dd($transactions);
        foreach($transactions as $transaction){
            $dbt = Transaction::where('type',$transaction['type'])->where('hash',$transaction['hash']);
            $address = Address::where('address',$transaction['address']);
            $address_id = null;
            $account_id = $this->id;
            $user_id = $this->user_id;

            if($address->count()){
                $address = $address->first();
                $address_id = $address->id;
            }

            if($dbt->count()==0){

                $dbt = new Transaction();
                $dbt->hash = $transaction['hash'];
                $dbt->currency = $transaction['currency'];
                $dbt->currency = $transaction['currency'];
                $dbt->amount = $transaction['amount'];
                $dbt->address_id = $address_id;
                $dbt->account_id = $account_id;
                $dbt->user_id = $user_id;
                $dbt->type = $transaction['type'];
                $dbt->confirmations = $transaction['confirmations'];
                $dbt->purpose = $transaction['purpose'];
                $dbt->is_stake = $transaction['is_stake'];
                $dbt->is_commission = $transaction['is_commission'];
                $dbt->is_earning =  $dbt->is_stake  &&  $dbt->is_commission?1:0;
                $dbt->save();

            }
        }
    }
    function sync_confirmations(){
       Transaction::where('account_id',$this->id)->where('confirmations','<',6)->chunk(100,function($transactions){
           foreach($transactions as $transaction){
              $bt = Bitcoin::gettransaction($transaction->hash) ;

              if(isset($bt['details']) && isset($bt['details']['category'])){
                  $transaction->type = $bt['details']['category'];

              }
               $transaction->confirmations = $bt['confirmations'];

               $transaction->save();

           }
        });


    }

    function sync_balance(){
        $this->balance = Bitcoin::getbalance($this->identifier);
        $this->save();
        $user = User::findOrFail($this->user_id);
        $user->update_activation();
    }
}
