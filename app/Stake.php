<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Stake extends Model
{
    //

    static function singalton($user_id){
        $date = Carbon::now()->format('Y-m-d');

        $stake = Stake::where('user_id',$user_id)->where('date',$date);
        if($stake->count()==0){
            $stake = new Stake();
            $stake->user_id = $user_id;

            $stake->date = $date;
            $stake->save();
        }

    }

    function user(){
        return $this->belongsTo('App\User');
    }
}
