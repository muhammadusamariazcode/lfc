<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    //
    static function comm_per($level){
        if($level == 1){
            return 0.40;
        }
        if($level == 2){
            return 0.30;
        }
        if($level == 3){
            return 0.20;
        }
        if($level == 4){
            return 0.15;
        }
        if($level == 5){
            return 0.14;
        }
        if($level == 6){
            return 0.13;
        }
        if($level == 7){
            return 0.12;
        }
        if($level == 8){
            return 0.11;
        }
        if($level == 9){
            return 0.10;
        }
        if($level == 10){
            return 0.09;
        }
        if($level == 11){
            return 0.08;
        }
        if($level == 12){
            return 0.07;
        }
        if($level == 13){
            return 0.06;
        }
        if($level == 14){
            return 0.05;
        }
        if($level == 15){
            return 0.04;
        }
        if($level == 16){
            return 0.03;
        }
        if($level == 17){
            return 0.02;
        }
        if($level == 18){
            return 0.01;
        }
        if($level == 19){
            return 0.01;
        }

        if($level == 20){
            return 0.01;
        }
        return 0;
    }
    static function singalton($parent_id,$stake_id,$amount){
        $date = Carbon::now()->format('Y-m-d');

        $user = User::find($parent_id);
        for($level = 1 ;$level<20;$level++){
            $level_parent  =$user->parentByLevel($level);
            if($level_parent==null){
                break;
            }
            if( $level_parent->status($level) == 'Active'){
                $comm = Commission::where('user_id',$level_parent->id)->where('stake_id',$stake_id)->where('date',$date);
                if($comm->count()==0){
                    $comm = new Commission();
                    $comm->user_id = $level_parent->id;
                    $comm->stake_id = $stake_id;
                    $comm->amount = Account::round( $amount * self::comm_per($level));
                    $comm->date = $date;
                    $comm->save();


                }
            }


        }


    }


    function paid(){

        try{
            if($this->status =='processed'){
                //make admin commision wallet trasaction to and make it paid


                if(User::is_exit($this->user_id)){
                    $user = User::find($this->user_id);
                    $account = $user->account();

                    $admin_stake_user = User::find(1);
                    $admin_stake_account = $admin_stake_user->account();
				sleep(10);
                    $tx_hash =    $admin_stake_account->send($account->default_address()->address,$this->amount,"Referral Commission",0);

                    $user->total_referral_commission = $user->total_referral_commission + $this->amount;
                    $user->save();

                    $this->tx_hash = $tx_hash;
                    $this->save();

                    $this->status = 'paid';
                    $this->save();
                }


            }

        }catch(\Exception $ex){
            echo $ex->getMessage()."\n";
        }

    }
}
