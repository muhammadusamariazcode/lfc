<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    //
    static function singleton($user_id, $parent_id, $level){
        $referral = Referral::where('user_id',$user_id)->where('parent_id',$parent_id);
        if($referral->count()){
            return $referral->first();
        }
        $referral = new Referral();
        $referral->user_id = $user_id;
        $referral->parent_id = $parent_id;
        $referral->level = $level;
        $referral->save();

        return $referral;
    }

    function user(){
        return $this->belongsTo('App\User');

    }

    function parent(){
        return $this->belongsTo('App\User');
    }
    static function countActiveReferral($parent_id,$level=1){

       return Referral::where('parent_id',$parent_id)->where('level',$level)->where('is_active',1)->count();

    }
}
