<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Api\ResponseFormat;
use App\User;
use Closure;
use Auth;
class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{


           if(!$request->has('user_id')){
               throw new \Exception("INVALID_ID_AND_AUTH_TOKEN");
           }

            if(!$request->has('auth_token')){
                throw new \Exception("INVALID_ID_AND_AUTH_TOKEN");
            }

            $user = User::select(['id','auth_token'])->where('id',$request->user_id);
           if($user->count() == 0){
               throw new \Exception("INVALID_ID_AND_AUTH_TOKEN");
           }

           $user = $user->first();
           if($user->auth_token != $request->auth_token){
               throw new \Exception("INVALID_ID_AND_AUTH_TOKEN");
           }


            return $next($request);

        }catch (\Exception $ex){
            return ResponseFormat::format([],'error',$ex->getMessage());
        }
    }
}
