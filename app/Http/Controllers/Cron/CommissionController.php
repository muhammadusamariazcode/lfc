<?php

namespace App\Http\Controllers\Cron;

use App\Commission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommissionController extends Controller
{
    //
    function paid(){
        Commission::chunk(100, function($commisions) { foreach ($commisions as $comm) {
             $comm->paid();
        } });
    }

}
