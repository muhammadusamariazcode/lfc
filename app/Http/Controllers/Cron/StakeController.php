<?php

namespace App\Http\Controllers\Cron;

use App\Account;
use App\Commission;
use App\Stake;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StakeController extends Controller
{
    //

    function generate(){
        User::where('id','!=',1)->chunk(100,function ($users){
            foreach($users as $user){
                if($user->status(0) == 'Active'){

                    Stake::singalton($user->id);

                }
            }
        });
    }
    function process(){
        Stake::chunk(100, function($stakes) { foreach ($stakes as $stake) {
                if($stake->status == 'pending'){
                    if( User::is_exit($stake->user_id)){
                        $user = User::find($stake->user_id);
                        if($user->status(0) == 'Active'){
                            $stake->status ='processed';
                            $stake->amount =Account::round( $stake->user->balance() * 0.01);

                            $stake->save();
                        }else{
                            $stake->status ='cancel';
                            $stake->save();
                        }

                    }

                }

        } });
    }

    function paid(){
        Stake::chunk(100, function($stakes) { foreach ($stakes as $stake) {

            try{


                if($stake->status == 'processed'){
                    //create transaction from admin stake wallet to it. if success then



                    if(User::is_exit($stake->user_id)){
                        $user = User::find($stake->user_id);
                        $account = $user->account();

                        $admin_stake_user = User::find(1);
                        $admin_stake_account = $admin_stake_user->account();
sleep(20);

                        $tx_hash =   $admin_stake_account->send($account->default_address()->address,$stake->amount,"Staking",0);

                        $stake->tx_hash = $tx_hash;
                        $stake->save();

                        $user->total_stake_earned = $user->total_stake_earned + $stake->amount;
                        $user->save();




                        Commission::singalton($stake->user_id,$stake->id,$stake->amount);

                    }
                    $stake->status ='paid';
                    $stake->save();

                    sleep(5);
                }


            }catch(\Exception $ex){
                echo $ex->getMessage()."\n";
            }
        } });
    }

}
