<?php

namespace App\Http\Controllers\Drivers;

use Illuminate\Http\Request;

use Mockery\Exception;

class Bitcoin
{

    protected $client = null;

    protected static $bitcoin =null;
    private function __construct()
    {
        $client  = new \Nbobtc\Http\Client(env('BITCOIN_CONNECTION_URL'));
        $this->client = $client;
    }



    static function singleton(){
        if(Bitcoin::$bitcoin == null){
            return Bitcoin::$bitcoin = new Bitcoin();
        }else{
            return Bitcoin::$bitcoin;
        }

    }

        static function run_command($command,$arguments = []){

       // self::wallet_unlock();
        $client =     self::singleton()->client;
        $command = new \Nbobtc\Command\Command($command,$arguments);
        $response = $client->sendCommand($command);


        $contents = $response->getBody()->getContents();
        $r = json_decode($contents,true);

        if(isset($r['error']) && $r['error']!=null){
            throw new \Exception($r['error']['message']);
        }
        return $r['result'];
    }
    //

    static function create($account_identifier){
        return  self::run_command('getaccountaddress',[$account_identifier]);
    }
    static function gettransaction($txid){
        return  self::run_command('gettransaction',[$txid]);
    }
    static function validateaddress($wallet_address)
    {
        return  self::run_command('validateaddress',[$wallet_address]);
    }
    static function sendfrom($account_identifier,$to_address,$amount){
        if(!self::isVaidAddress($to_address)){
            throw new \Exception("Invalid Address");
        }

        $amount =$amount+0;
//dd($amount);
        $balance = self::getbalance($account_identifier);
        if($amount > $balance){
            throw new \Exception("Insufficient Balance");
        }
        self::wallet_unlock();

        return  self::run_command('sendfrom',[$account_identifier,$to_address,$amount]);

    }


    static function getbalance($account_identifier){
        return  self::run_command('getbalance',[$account_identifier]);

    }

    static function listtransactions($account_identifier,$count=10,$from=0){
        return  self::run_command('listtransactions',[$account_identifier,$count,$from]);

    }


    static function export($wallet_address)
    {
        try{
            self::wallet_unlock();
            return  self::run_command('dumpprivkey',[$wallet_address]);

        }catch (\Exception $ex){

        }
          }


    private  static function wallet_unlock(){

        try{
       // return  self::run_command('walletpassphrase',[env('BITCOIN_WALLET_UNLOCK'),300]);
        }catch (\Exception $ex){

        }
    }

    private  static function wallet_lock(){
        return  self::run_command('walletlock');
    }

    static function walletpassphrasechange(){
        //  return self::run_command('walletpassphrasechange',['pre','new']);
    }
    static function encryptWallet(){
          return self::run_command('encryptwallet',[env('BITCOIN_WALLET_UNLOCK')]);
    }

    private static function convertToBTCFromSatoshi($value){
        return bcdiv( intval($value), 100000000, 8 );
    }





    static function  isVaidAddress($address){
     return true;
      //  return AddressValidator::isValid($address);
    }


    static function getblockcount(){
        return  self::run_command('getblockcount');

    }




    //sending
}
