<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResponseFormat extends Controller
{
    //
    static function format($data,$status = 'success',$message=''){
        $r['status'] = $status;
        $r['message'] = $message;
        $r['data'] = $data;

        return response(json_encode($r));
    }

    static function format2($user,$balance,$transactions,$message){
        return self::format([
            'user' =>$user,
            'balance' =>$balance,
            'transactions' =>$transactions
        ],'success',$message);
    }

}
