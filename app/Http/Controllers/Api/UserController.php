<?php

namespace App\Http\Controllers\Api;

use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('ApiAuth',['except'=>['login','register']]);
    }

    function login(Request $request){
        try{

            $user = User::ApiLogin($request->email,$request->password);
            return ResponseFormat::format2(User::ApiUserR($user),User::ApiBalance($user),[],'LOGIN_SUCCESS');

        }catch (\Exception $ex){
            return ResponseFormat::format([],'error',$ex->getMessage());
        }

    }

    function register(Request $request){
        try{


            $user = User::ApiRegister($request->email,$request->name,$request->password);
            return ResponseFormat::format2(User::ApiUserR($user),User::ApiBalance($user),[],'REGISTER_SUCCESS');

        }catch (\Exception $ex){
            return ResponseFormat::format([],'error',$ex->getMessage());
        }

    }

    function balance(Request $request){
        try{

            $user = User::ApiUser($request);

            return ResponseFormat::format2(User::ApiUserR($user),User::ApiBalance($user),[],'BALANCE_FETCH_SUCCESS');

        }catch (\Exception $ex){
            return ResponseFormat::format([],'error',$ex->getMessage());
        }

    }


    function transactions(Request $request){
        try{
            $user = User::ApiUser($request);
            $transactions = Transaction::where('user_id',$user->id)->orderBy('id','desc')->paginate(20);
            return ResponseFormat::format2(User::ApiUserR($user),User::ApiBalance($user),$transactions,'TRANSACTION_FETCH_SUCCESS');

        }catch (\Exception $ex){
            return ResponseFormat::format([],'error',$ex->getMessage());
        }
    }



    function send(Request $request){
        try{
            $user = User::ApiUser($request);
            $user->account()->send($request->address,$request->amount);

            return ResponseFormat::format2(User::ApiUserR($user),User::ApiBalance($user),[],'SEND_SUCCESS');

        }catch (\Exception $ex){
            return ResponseFormat::format([],'error',$ex->getMessage());
        }
    }

}
