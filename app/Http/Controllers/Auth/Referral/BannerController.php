<?php

namespace App\Http\Controllers\Auth\Referral;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        return view('auth.referral.banner.home');
    }
    //
}
