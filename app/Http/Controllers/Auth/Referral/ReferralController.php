<?php

namespace App\Http\Controllers\Auth\Referral;

use App\Referral;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferralController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        $referrals = Referral::where('parent_id',\Auth::user()->id)->orderBy('id','desc')->paginate(10);

        return view('auth.referral.home',compact('referrals'));
    }
    function table(Request $request,$level){
        if($level >0 && $level <=env('TOTAL_LEVEL')){
            $referrals = Referral::where('parent_id',\Auth::user()->id)->where('level',$level)->orderBy('id','asc')->paginate(5);
            return view('auth.referral._table',compact('referrals'));
        }


    }
}
