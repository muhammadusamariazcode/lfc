<?php

namespace App\Http\Controllers\Auth\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        $user = \Auth::user();
        return view('auth.profile.home',compact('user'));
    }
    function update(Request $request){
       $validator =  Validator::make($request->all(), [
            'name' => 'required|string|max:255',

       ]);

       $validator->validate();


       $user = \Auth::user();
       $user->name = $request->name;
       $user->save();

       return redirect()->back();

    }
    function update_password(Request $request){
        $validator =  Validator::make($request->all(), [
            'current_password' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',

        ]);

        if($validator->fails()){
            toast()->error($validator->errors()->first());
            return redirect()->back();

        }

        if(!Hash::check($request->current_password,\Auth::user()->password)){
            toast()->error("Invalid Current Password");
            return redirect()->back();

        }




        $user = \Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();
        toast()->error("Password Updated Successfully");

        return redirect()->back();

    }
}
