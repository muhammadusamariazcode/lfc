<?php

namespace App\Http\Controllers\Auth\Security;



use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;
use PragmaRX\Google2FA\Google2FA;

class Google2faController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth',['except'=>['ask','check']]);
    }

    function image($user=null){


        //function url http://localhost/auth/google2fa/image
        try
        {
            if(empty($user)){
                $user = \Auth::user();
            }



            $r['status'] = 'success';
            $r['url'] =  $user->google2fa_secret_url();
            return $r ;
        }catch (\Exception $ex)
        {
            $r['status'] = 'error';
            $r['message'] =$ex->getMessage();
        }
    }

    function enable_ajax(Request $request){
        try
        {

            $this->verify($request->all());
            $user = \Auth::user();
            $user->is_google2fa = 1;
            $user->save();
            session(['is_google_2fa_session'=> true]);

            $r['status'] = 'success';
            $r['message'] = 'Google2fa enable';
            return $r;


        }catch (\Exception $ex){
            $r['status'] = 'error';
            $r['message'] = $ex->getMessage();
            return $r;
        }

    }
    function disable_ajax(Request $request){
        try
        {

            $this->verify($request->all());
            $user = \Auth::user();
            $user->is_google2fa = 0;
            $user->save();
            session(['is_google_2fa_session'=> true]);

            $r['status'] = 'success';
            $r['message'] = 'Google2fa disable';
            return $r;


        }catch (\Exception $ex){
            $r['status'] = 'error';
            $r['message'] = $ex->getMessage();
            return $r;
        }

    }

    function toggle(Request $request){

        try
        {
            $this->verify($request->all());
            $user = \Auth::user();
            $user->is_google2fa = ! $user->is_google2fa ;
            $user->save();
            if($user->is_google2fa){
                toast()->success("Successfully Enabled");

            }else{
                $user->google2fa_secret ='';
                    $user->save();
                toast()->success("Successfully Disabled");

            }
            return back();

        }catch (\Exception $ex)
        {
            toast()->error($ex->getMessage(),"ERROR");
            return back();
        }

    }

    function verify($data){

        $validator = Validator::make($data,[
                'secret' => 'required|string|max:255',
            ]
        );
        if($validator->fails()){
            throw new \Exception($validator->errors()->first());
        }

        $user = \Auth::user();
        $google2fa = new Google2FA();
        $secret = $data['secret'];

        $window = 8; // 8 keys (respectively 4 minutes) past and future

        $valid = $google2fa->verifyKey($user->google2fa_secret, $secret, $window);
        if(!$valid){
            throw new \Exception("Invalid Secret key");
        }
    }


    function index(Request $request){

        $user = \Auth::user();

        return view('auth.google2fa',compact('user'));
    }
    function indexHome(Request $request){

        return view('auth.google2faverify');
    }
    function check(Request $request){

        try{
            if(!\Auth::guest() && \Auth::user()->is_google2fa) {
                $this->verify($request->all());
                session(['is_google_2fa_session' => true]);
                return redirect()->route('home');
            }
        }catch (\Exception $ex){
            toast()->error($ex->getMessage(),"ERROR");
            return back();
        }
    }
    function check_ajax(Request $request){
        try{
            $this->verify($request->all());
            session(['is_google_2fa_session'=> true]);
            $r['status'] = 'success';
            $r['message'] = 'succefully verified';
            return $r;

        }catch (\Exception $ex){
            $r['status'] = 'error';
            $r['message'] = $ex->getMessage();
            return $r;
        }

    }



    function ask(Request $request){

        if(!\Auth::guest() && \Auth::user()->is_google2fa){

            return view('auth.google2fa_ask',['user'=>\Auth::user()]);

        }
    }
}