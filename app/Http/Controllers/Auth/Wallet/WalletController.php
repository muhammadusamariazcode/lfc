<?php

namespace App\Http\Controllers\Auth\Wallet;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WalletController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth');
    }

    function index(){
        $transactions = Transaction::where('user_id',\Auth::user()->id)->orderBy('id','desc')->paginate(10);
        $title = 'Transaction History';
        return view('auth.wallet.home',compact('transactions','title'));
    }

    function store(Request $request){
        try{
            $user = \Auth::user();
            $account = $user->account();
            $account->send($request->address,$request->amount);

            toast()->success("Amount sended successfully");
            return redirect()->route('auth.wallet.index');
        }catch (\Exception $ex){
            toast()->error($ex->getMessage());
            return redirect()->back();
        }
    }
}
