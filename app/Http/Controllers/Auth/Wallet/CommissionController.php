<?php

namespace App\Http\Controllers\Auth\Wallet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;

class CommissionController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth');
    }
    function index()
    {
        $transactions = Transaction::where('user_id',\Auth::user()->id)->where('is_commission',1)->orderBy('id','desc')->paginate(10);
        $title = 'Referral Commission History';
        return view('auth.wallet.home',compact('transactions','title'));

    }

    //
}
