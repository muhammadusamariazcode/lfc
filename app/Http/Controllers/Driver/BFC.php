<?php

namespace App\Http\Controllers\Driver;

use App\Account;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use App\Transaction;
class BFC extends Controller
{
    //
    static function create($identifier){
        $dwallet['address'] = "";
        $dwallet['private_key'] = "";
        $account = Account::where('identifier',$identifier)->first();
        $dwallet['address'] = $account->user->email;
        return $dwallet;
    }
    static function send($identifier,$to,$amount,$purpose=''){


        $data['address'] = $to;
        $data['amount'] = $amount;
        if(!empty($purpose)){
            $validator = Validator::make($data, [

                'address' => 'required|string|email|max:255'
            ]);
        }else{
            $validator = Validator::make($data, [

                'address' => 'required|string|email|max:255',
                'amount' => 'required|numeric|min:10',
            ]);

            if( $amount <10  ){
                throw new \Exception("You can send min 10");
            }

        }

        if($validator->fails()){
            throw new Exception($validator->errors()->first());
        }


        $accountS = Account::where('identifier',$identifier)->first();
        if($accountS->balance < $amount  ){
            throw new \Exception("Insufficient Balance");
        }




        $userS = $accountS->user;


        $userR = User::where('email',$to);
        if($userR->count()){
            $userR = $userR->first();
        }else{
            throw new \Exception("Email not found");

        }
        $accountR = Account::singalton($userR->id,'bfc');

        if($accountR->default_address()->address == $accountS->default_address()->address){
         //   throw new \Exception("You cannot send to your self");
        }

        $transactionS = new Transaction();
        $transactionR = new Transaction();

        $transactionR->currency = $transactionS->currency = 'bfc';
        $transactionR->amount = $transactionS->amount = $amount;
        $transactionS->account_id= $accountS->id;
        $transactionS->user_id= $accountS->user_id;

        $transactionS->address_id= $accountS->default_address()->id;
        $transactionR->account_id= $accountR->id;
        $transactionR->user_id= $accountR->user_id;
        $transactionR->address_id = $accountR->default_address()->id;


        $transactionS->type = 'send';
        $transactionR->type = 'receive';
        $transactionS->confirmations = $transactionR->confirmations = 3;
        $transactionS->purpose = $transactionR->purpose = $purpose;
        if($purpose == 'Staking'){
            $transactionS->is_stake = $transactionR->is_stake  = 1;
            $transactionS->is_earning = $transactionR->is_earning  = 1;

        }elseif($purpose == 'Referral Commission'){
            $transactionS->is_commission = $transactionR->is_commission  = 1;
            $transactionS->is_earning = $transactionR->is_earning  = 1;

        }

        $transactionS->save();
        $transactionR->save();

        $transactionS->hash = $transactionR->hash = $transactionS->id."_".$transactionR->id;
        $transactionS->save();
        $transactionR->save();
        $transactionS->hash ;


        $accountS = Account::singalton($userS->id,'bfc');

        $accountS->balance = $accountS->balance - $amount;
        $accountS->save();



        $accountR = Account::singalton($userR->id,'bfc');

        $accountR->balance = $accountR->balance + $amount;

        $accountR->save();

        $userS->update_activation();


        $userR->update_activation();


        return   $transactionS->hash;


    }
}
