
@extends('layouts.frontend')

@section('content')

    <section class="slider-section">
        <div class="slider-container">
            <div class="container container-sm">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <div class="carousel-indicators-wrp text-center">
                    </div>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner-wrp">
                        <div class="carousel-inner" role="listbox">

                            <div class="item active">
                                <div class="row row-table">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="img-wrp text-center">
                                            <img src="{{url('2222.png')}}" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <label class="h1 title inverse text-uppercase">
                                             BitFil Coin

                                        </label>
                                        <p class="intro">
                                            The  first intelligence cryptocurrency of the people for the people and by the people Download your wallet and start your future with {{env('APP_NAME')}}                                        </p>
                                        <a href="{{route('login')}}" data-event="webWallet" target="_blank" class="wallets_metrika btn btn-primary btn-inverse text-normal"> open <i class="fa fa-long-arrow-right fa-long-arrow-right-ed"></i></a>
                                    </div>
                                </div>
                            </div>

                        </div><!-- carousel-inner -->

                    </div><!-- carousel-inner-wrp -->
                </div>
            </div>
        </div>
    </section>
    <section class="slider-section">
        <div class="slider-container">
            <div class="container container-sm">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <div class="carousel-indicators-wrp text-center">
                    </div>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner-wrp">
                        <div class="carousel-inner" role="listbox">

                            <div class="item active">
                                <div class="row row-table">

                                    <div class="col-xs-12 col-sm-6">
                                        <label class="h1 title inverse text-uppercase">
                                            Why BitFil ?

                                        </label>
                                        <p class="intro">
                                            Because of this there is no package buy, only hold your amount in shape of {{env('COIN_SYMBOL')}} and get daily 0.7% staking on yours hold amount of {{env('COIN_SYMBOL')}}.

                                    </div>

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="img-wrp text-center">
                                            <img src="{{url('111.jpeg')}}" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div><!-- carousel-inner -->

                    </div><!-- carousel-inner-wrp -->
                </div>
            </div>
        </div>
    </section>
@stop