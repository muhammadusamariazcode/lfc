@extends('layouts.frontend')


@section('content')

    <section class="slider-section">
        <div class="slider-container slide-static">
            <div class="container container-sm">
                <div class="item item-inverse fixed-img text-bottom">
                    <label class="h0 title inverse text-uppercase pull-right">
                        Mass media about <br> BitFil Coin                </label>
                    <img src="{{url('frontend/10.png')}}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="content-section section-white">
        <div class="container container-sm">
            <div class="row row-list">
                <div class="row-list-content text-center">

                    <ul class="list-inline">
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.mosaiquefm.net</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.mosaiquefm.net/fr/communiques-de-presse-tunisie/26606/la-crypto-monnaie-p2p-BitFil Coin-asia.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.finance.yahoo.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://finance.yahoo.com/news/BitFil Coin-coin-revolutionary-generation-045600794.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">cryptocoinsnews.com</p>
                                    <a class="btn btn-primary btn-xs" href="https://www.cryptocoinsnews.com/new-money-net/" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.pulse.ng</p>
                                    <a class="btn btn-primary btn-xs" href="http://pulse.ng/gist/BitFil Coin-coin-event-in-nigeria-dynamics-of-the-cryptocurrency-developing-with-blockchain-technology-id5499074.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.dailypost.ng</p>
                                    <a class="btn btn-primary btn-xs" href="http://dailypost.ng/2016/09/16/BitFil Coin-coin-event-nigeria-dynamics-cryptocurrency-developing-blockchain-technology/" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.naij.com</p>
                                    <a class="btn btn-primary btn-xs" href="https://www.naij.com/966597-cryptocurrency-value-money.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.kapitalis.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://kapitalis.com/tunisie/2016/09/10/e-dinarcoin-la-nouvelle-crypto-monnaie-pourrait-transformer-leconomie" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.adabasini.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.adabasini.com/mobil/haber.php?id=398513" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.kamusaati.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.kamusaati.com/ekonomi/BitFil Coin-coin-yeni-nesil-kripto-para-dijital-para-dunyasina-damga-vuracak-h13428.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">blog.chron.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://blog.chron.com/storystudio/2016/09/BitFil Coin-coin-revolutionary-new-generation-cryptocurrency/" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.egitimajansi.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.egitimajansi.com/haber/BitFil Coin-coin-yeni-nesil-kripto-para-dijital-para-dunyasina-damga-vuracak-haberi-34357h.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.manisagundemi.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.manisagundemi.com/BitFil Coin-coin-yeni-nesil-kripto-para-dijital-para-dunyasina-damga-vuracak-832609.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.murekkephaber.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.murekkephaber.com/BitFil Coin-coin-yeni-nesil-kripto-para-dijital-para-dunyasina-damga-vuracak/4230/" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.konyadagundem.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.konyadagundem.com/145414-BitFil Coin-coin-yeni-nesil-kripto-para-dijital-para-dunyasina-damga-vuracak.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.fintechnews.sg</p>
                                    <a class="btn btn-primary btn-xs" href="http://fintechnews.sg/5011/blockchain/BitFil Coin-coin-revolutionary-new-generation-cryptocurrency/" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.tucsonnewsnow.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.tucsonnewsnow.com/story/32747141/BitFil Coin-coin-revolutionary-new-generation-cryptocurrency" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.news9.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.news9.com/story/32747141/BitFil Coin-coin-revolutionary-new-generation-cryptocurrency" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.wfsb.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.wfsb.com/story/32747141/BitFil Coin-coin-revolutionary-new-generation-cryptocurrency" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">www.nbc12.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.nbc12.com/story/32747141/BitFil Coin-coin-revolutionary-new-generation-cryptocurrency" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">wmcactionnews5.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.wmcactionnews5.com/story/32747141/BitFil Coin-coin-revolutionary-new-generation-cryptocurrency" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">pulse.ng</p>
                                    <a class="btn btn-primary btn-xs" href="http://pulse.ng/gist/cryptocurrency-how-to-get-the-best-value-for-your-money-id5531667.html" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">cwt.top</p>
                                    <a class="btn btn-primary btn-xs" href="http://cwt.top/news/143/cwt-indonesia-give-relief-for-flood-victims-in-cimacan" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">cointelegraph.com</p>
                                    <a class="btn btn-primary btn-xs" href="https://www.cointelegraph.com/news/new-money-on-the-net" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">ghanaweb.com</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.ghanaweb.com/GhanaHomePage/business/The-BitFil Coin-Coin-presentation-in-Ghana-480884" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">dailypost.ng</p>
                                    <a class="btn btn-primary btn-xs" href="http://dailypost.ng/2016/10/24/BitFil Coin-coin-presented-nigerians-october-26/?utm_source=Viber&amp;utm_medium=Chat&amp;utm_campaign=Private" target="_blank">Read</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="./" alt="..."></div>
                                <div class="caption">
                                    <p class="link">timeslive.co.za</p>
                                    <a class="btn btn-primary btn-xs" href="http://www.timeslive.co.za/featuredpartner/2016/11/02/BitFil Coin-Coin-a-new-financial-instrument" target="_blank">Read</a>            </div><!-- row-list-content -->
                            </div>
                        </li></ul></div>
            </div></div></section>


@stop