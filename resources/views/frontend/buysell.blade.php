@extends('layouts.frontend')

@section('content')

    <section class="slider-section">

        <div class="slider-container slide-static">
            <div class="container container-sm">
                <div class="item item-inverse fixed-img text-bottom">
                    <label class="h0 title inverse text-uppercase pull-right">
                        Buy <br> {{env('APP_NAME')}}                </label>
                    <img src="{{url('frontend/7.png')}}" alt="">
                </div>
            </div>
        </div>


    </section>

    <section class="content-section section-white">
        <div class="container container-sm">
            <div class="row row-list">

                <div class="row-list-content text-center">

                    <ul class="list-inline">


                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="{{url('frontend/yb.png')}}" alt="..." /></div>
                                <div class="caption">
                                    <p class="link">www.yobit.net</p>
                                    <a class="btn btn-primary btn-xs wallets_metrika" href="#" target="_blank" data-event="buyYobit.net">Buy</a>

                                </div>
                            </div></li>
                                               <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="https://bittrex.com/Content/img/Lineage/bittrex-logo-mark.svg" alt="..." /></div>
                                <div class="caption">
                                    <p class="link">Bittrex.com</p>
                                    <a class="btn btn-primary btn-xs wallets_metrika" href="#" target="_blank" data-event="buyYobit.net">Buy</a>

                                </div>
                            </div></li>


                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="https://cryptohub.online/static/img/solar-energy.png" alt="..." /></div>
                                <div class="caption">
                                    <p class="link">Cryptohub.online</p>
                                    <a class="btn btn-primary btn-xs wallets_metrika" href="#" target="_blank" data-event="buyYobit.net">Buy</a>

                                </div>
                            </div></li>
                        <li class="col-xs-12 col-sm-3">
                            <div class="thumbnail">
                                <div class="img-wrp"><img src="https://www.cryptopia.co.nz/Content/Images/Cryptopia-Light2x.png" alt="..." /></div>
                                <div class="caption">
                                    <p class="link">Cryptopia.co.nz</p>
                                    <a class="btn btn-primary btn-xs wallets_metrika" href="#" target="_blank" data-event="buyYobit.net">Buy</a>

                                </div>
                            </div></li>

                    </ul>            </div><!-- row-list-content -->

            </div>
        </div>
    </section>

@stop