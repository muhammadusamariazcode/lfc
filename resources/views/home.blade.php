@extends('layouts.backend')

@section('page.title','Dashboard')
@section('content')

    <main class="app-content">
        @include('_alert_google_2fs')

        @include('auth._balance')


        <div class="row">

            <div class="col-md-3">

                <table class="table table-striped bg-light">

                    <thead class="" style="background-color: #d2d5da;">
                    <tr>
                        <th colspan="2">Commision Plan</th>


                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Active User</td>
                        <td class="text-right"><span class="text-muted">1% staking</span></td>
                    </tr>
                    <tr>
                        <td>Level 1</td>
                        <td class="text-right"><span class="text-muted">40%</span></td>
                    </tr>
                    <tr>
                        <td>Level 2</td>
                        <td class="text-right"><span class="text-muted">30%</span></td>
                    </tr>
                    <tr>
                        <td>Level 3</td>
                        <td class="text-right"><span class="text-muted">20%</span></td>
                    </tr>
                    <tr>
                        <td>Level 4</td>
                        <td class="text-right"><span class="text-muted">15%</span></td>
                    </tr>
                    <tr>
                        <td>Level 5</td>
                        <td class="text-right"><span class="text-muted">14%</span></td>
                    </tr>
                    <tr>
                        <td>Level 6</td>
                        <td class="text-right"><span class="text-muted">13%</span></td>
                    </tr>
                    <tr>
                        <td>Level 7</td>
                        <td class="text-right"><span class="text-muted">12%</span></td>
                    </tr>
                    <tr>
                        <td>Level 8</td>
                        <td class="text-right"><span class="text-muted">11%</span></td>
                    </tr>
                    <tr>
                        <td>Level 9</td>
                        <td class="text-right"><span class="text-muted">10%</span></td>
                    </tr>
                    <tr>
                        <td>Level 10</td>
                        <td class="text-right"><span class="text-muted">9%</span></td>
                    </tr>
                    <tr>
                        <td>Level 11</td>
                        <td class="text-right"><span class="text-muted">8%</span></td>
                    </tr>
                    <tr>
                        <td>Level 12</td>
                        <td class="text-right"><span class="text-muted">7%</span></td>
                    </tr>
                    <tr>
                        <td>Level 13</td>
                        <td class="text-right"><span class="text-muted">6%</span></td>
                    </tr>
                    <tr>
                        <td>Level 14</td>
                        <td class="text-right"><span class="text-muted">5%</span></td>
                    </tr>
                    <tr>
                        <td>Level 15</td>
                        <td class="text-right"><span class="text-muted">4%</span></td>
                    </tr>
                    <tr>
                        <td>Level 16</td>
                        <td class="text-right"><span class="text-muted">3%</span></td>
                    </tr>
                    <tr>
                        <td>Level 17</td>
                        <td class="text-right"><span class="text-muted">2%</span></td>
                    </tr>
                    <tr>
                        <td>Level 18</td>
                        <td class="text-right"><span class="text-muted">1%</span></td>
                    </tr>
                    <tr>
                        <td>Level 19</td>
                        <td class="text-right"><span class="text-muted">1%</span></td>
                    </tr>
                    <tr>
                        <td>Level 20</td>
                        <td class="text-right"><span class="text-muted">1%</span></td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="col-md-9">
                <div id="level_status">
                    <div class=" loader"></div>

                </div>
            </div>

        </div>
    </main>

@endsection
@section('scripts')
    <script>
        $('#level_status').load("{{route('status')}}");
    </script>
@stop