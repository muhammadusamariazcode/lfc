<table class="table table-striped bg-light">

    <thead class="" style="background-color: #d2d5da;">
    <tr>
        <th colspan="3">LEVEL STATUS</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Active User</td>
        <td>Need 1000 {{env('COIN_SYMBOL')}} in own wallet </td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(0) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(0) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 1</td>
        <td>Need 1000 {{env('COIN_SYMBOL')}} in own wallet and at least 5 active members in Level 1</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(1) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(1) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 2</td>
        <td>Need Level 1 Active with 2000 {{env('COIN_SYMBOL')}} in own wallet and at least 15 active members in Level 2</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(2) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(2) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 3</td>
        <td>Need Level 2 Active with 3000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 3</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(3) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(3) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 4</td>
        <td>Need Level 3 Active with 4000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 4</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(4) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(4) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 5</td>
        <td>Need Level 4 Active with 5000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 5</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(5) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(5) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 6</td>
        <td>Need Level 5 Active with 6000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 6</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(6) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(6) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 7</td>
        <td>Need Level 6 Active with 7000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 7</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(7) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(7) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 8</td>
        <td>Need Level 7 Active with 8000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 8</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(8) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(8) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 9</td>
        <td>Need Level 8 Active with 9000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 9</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(9) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(9) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 10</td>
        <td>Need Level 9 Active with 10000{{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 10</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(10) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(10) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 11</td>
        <td>Need Level 10 Active with 11000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 11</td>
        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(11) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(11) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 12</td>
        <td>Need Level 11 Active with 12000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 12</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(12) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(12) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 13</td>
        <td>Need Level 12 Active with 13000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 13</td>
        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(13) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(13) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 14</td>
        <td>Need Level 13 Active with 14000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 14</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(14) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(14) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 15</td>
        <td>Need Level 14 Active with 15000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 15</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(15) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(15) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 16</td>
        <td>Need Level 15 Active with 16000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 16</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(16) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(16) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 17</td>
        <td>Need Level 16 Active with 17000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 17</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(17) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(17) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 18</td>
        <td>Need Level 17 Active with 18000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 18</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(18) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(18) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 19</td>
        <td>Need Level 18 Active with 19000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 19</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(19) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(19) }}</span>
        </td>
    </tr>
    <tr>
        <td>Level 20</td>
        <td>Need Level 19 Active with 20000 {{env('COIN_SYMBOL')}} in own wallet and at least 1 active members in Level 20</td>

        <td class="text-right">
            <span class="btn  btn-sm py-1 mt-2 @if(\Auth::user()->status(20) =='Active') px-4 btn-warning @else btn-dark @endif  ">{{\Auth::user()->status(20) }}</span>
        </td>
    </tr>
    </tbody>
</table>



