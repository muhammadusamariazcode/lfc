@extends('layouts.backend')
@section('page.title','Profile')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <form class="card" action="{{route('auth.profile.update')}}" method="POST">
                    @csrf
                    <div class="card-body">
                        <h3 class="card-title">Edit Profile</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Email</label>
                                    <input type="text" readonly disabled  class="form-control" placeholder="Email" value="{{$user->email}}">
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="form-label"> Name</label>
                                    <input name="name"   type="text" class="form-control" placeholder=" Name" value="{{$user->name}}" required  >
                                </div>
                            </div>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif


                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Update Profile</button>
                    </div>
                </form>
                <form class="card" action="{{route('auth.profile.update_password')}}" method="POST">
                    @csrf
                    <div class="card-body">
                        <h3 class="card-title">Update Password</h3>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Current Password</label>
                                    <input type="password"  name="current_password"  class="form-control" placeholder="Current Password" value="" required>
                                </div>
                            </div>
                            @if ($errors->has('current_password'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('current_password') }}</strong>
                                    </span>
                            @endif
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">New Password</label>
                                    <input type="password"  name="password"  class="form-control" placeholder="New Password" value="" required>
                                </div>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Confirm new Password</label>
                                    <input type="password"  name="password_confirmation"  class="form-control" placeholder="Confirm new Password" value="" required>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Update Password</button>
                    </div>
                </form>

            </div>
        </div>
    </div>@endsection
