@extends('layouts.backend')
@section('page.title','Profile')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <form class="card" action="{{route('auth.google2fa.check')}}" method="POST" >
                    @csrf
                    <div class="card-body">
                        <h3 class="card-title">Google 2fa Security</h3>
                        <div class="row">

                                    <div class=" col-md-12 ">
                                       <p>Please Open Google Authenticator Application and type the code</p>
                                        <div class="form-group">
                                            <label class="form-label">Secret Code</label>
                                            <input name="secret" type="text" class="form-control" placeholder="Secret Code" value="{{$user->first_name}}"  required>
                                        </div>

                                </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary"> Verify Google 2fa Session</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
@section('scripts')

@stop