@extends('layouts.auth')

@section('content')
    <div class="page"  style="padding-top: 5%;">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-login col-md-6  mx-auto">

                        <form class="card" method="POST" action="{{ route('password.email') }}">
                            <div class="text-center mb-6" style="padding-top: 5%;">
                                <img src="/assets/frontend/img/logo_.png" class="h-6"  style="max-width: 80%;" alt="">
                            </div>
                            @csrf
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="card-body p-6">
                                <div class="card-title">Forgot password</div>
                                <p class="text-muted">Enter your email address and your password will be reset and emailed to you.</p>
                                <div class="form-group">
                                    <label class="form-label" for="exampleInputEmail1">Email address</label>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif                            </div>
                                <div class="form-footer">
                                    <button type="submit" class="btn btn-primary btn-block">Send me new password</button>
                                </div>
                            </div>
                        </form>
                        <div class="text-center text-muted">
                            Forget it, <a href="{{route('login')}}">send me back</a> to the sign in screen.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
