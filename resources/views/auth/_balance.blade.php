<div class="row">
    <div class="col-md-6 col-lg-3" onclick="window.open('{{route('auth.wallet.index')}}','_self')">
        <div class="widget-small primary coloured-icon p-2"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
                <h4>{{\Auth::user()->balance()}}</h4>
                <p><b>Balance</b></p>
                <h4 class="bfc-head">{{env('COIN_SYMBOL')}}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3" onclick="window.open('{{route('auth.wallet.staking')}}','_self')">
        <div class="widget-small info coloured-icon p-2"><i class="icon fa fa-thumbs-o-up fa-3x"></i>
            <div class="info">
                <h4>{{\Auth::user()->total_stake_earned}}</h4>
                <p><b> Staking</b></p>
                <h4 class="bfc-head">{{env('COIN_SYMBOL')}}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3" onclick="window.open('{{route('auth.wallet.commission')}}','_self')">
        <div class="widget-small warning coloured-icon p-2"><i class="icon fa fa-files-o fa-3x"></i>
            <div class="info">
                <h4>{{\Auth::user()->total_referral_commission }}</h4>
                <p><b> Commission</b></p>
                <h4 class="bfc-head">{{env('COIN_SYMBOL')}}</h4>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3" onclick="window.open('{{route('auth.wallet.earning')}}','_self')">
        <div class="widget-small danger coloured-icon p-2"><i class="icon fa fa-star fa-3x"></i>
            <div class="info">
                <h4>{{\Auth::user()->total_stake_earned + \Auth::user()->total_referral_commission}}</h4>
                <p><b> Earning</b></p>
                <h4 class="bfc-head">{{env('COIN_SYMBOL')}}</h4>
            </div>
        </div>
    </div>
</div>
