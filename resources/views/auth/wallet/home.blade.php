@extends('layouts.backend')

@section('page.title','Transaction History')
@section('content')
    <main  class="app-content">

        @include('auth._balance')

        <div class="row row-cards row-deck">
            <div class="col-sm-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">{{$title}}</h2>
                        <div class="card-options">
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                            <thead>
                            <tr>
                                <th class="text-center w-1"></th>
                                <th>Transaction</th>
                                <th>Description</th>

                                <th class="text-center">Confirmations</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($transactions as $transaction)
                                <tr>
                                    <td class="text-center">
                                        <div class="avatar d-block" >
                                            <i class="fe @if($transaction->type=='send') fe-send @else fe-download  @endif" style="color:#0a0c0d"></i>
                                        </div>
                                    </td>
                                    <td>
                                        <div>{{$transaction->amount}}</div>
                                        <div class="small text-muted">
                                            {{$transaction->type}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="clearfix">
                                            <div>@if(!empty($transaction->purpose)) {{$transaction->purpose}} @else {{$transaction->type}} @endif</div>
                                            <div class="small text-muted">
                                                Timestamp:
                                                {{$transaction->created_at}} UTC
                                                @if($transaction->confirmations>=1)  <a href="http://explorer.bitfilcoin.com/tx/{{$transaction->hash}}" target="_blank" ><i class="fe fe-external-link"></i></a>@endif
                                            </div>
                                        </div>

                                    </td>


                                    <td class="text-center">
                                        <div class="mx-auto chart-circle chart-circle-xs" data-value="@if($transaction->confirmations>6)100@else{{$transaction->confirmations/6 * 100}}@endif" data-thickness="3" data-color="blue"><canvas width="40" height="40"></canvas>
                                            <div class="chart-circle-value">@if($transaction->confirmations>=6) confirmed @else {{$transaction->confirmations}}/6 @endif</div>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <center>
                            {!! $transactions->links() !!}
                        </center>
                    </div>

                </div>
            </div>

        </div>
    </main>



@endsection
