@extends('layouts.backend')
@section('page.title','Profile')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <form class="card" action="{{route('auth.google2fa.toggle')}}" method="POST" >
                    @csrf
                    <div class="card-body">
                        <h3 class="card-title">Google 2fa Security</h3>
                        <div class="row">
                            @if($user->is_google2fa == 0)
                                    <div class="col-md-12">
                                        <div class="visible-print text-center">
                                            <div class="qr-code-img">

                                            <img  src="{{$user->google2fa_inline()}}" />

                                            </div>
                                            <p>Scan and tell the secret key.</p>
                                        </div>
                                    </div>
                            @endif
                                    <div class=" col-md-12 ">
                                        <div class="form-group">
                                            <label class="form-label">Secret Code</label>
                                            <input name="secret" type="text" class="form-control" placeholder="Secret Code" value="{{$user->first_name}}"  required>
                                        </div>

                                </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary"> @if($user->is_google2fa == 0)Enable @else Disable @endif Google 2fa</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
@section('scripts')

@stop