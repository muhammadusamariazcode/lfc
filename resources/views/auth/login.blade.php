@extends('layouts.auth')
@section('page.title','Login')

@section('content')
    <div class="page" style="padding-top: 5%;">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-login col-md-6 mx-auto">

                        <form class="card" method="POST" action="{{ route('login') }}">
                            <div class="text-center mb-6" style="padding-top: 5%;">
                                <img src="/assets/frontend/img/logo_.png" class="h-6"  style="max-width: 80%;" alt="">
                            </div>
                            @csrf
                            <div class="card-body p-6">
                                <div class="card-title">Login to your account</div>
                                <div class="form-group">
                                    <label class="form-label">Email address</label>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif                            </div>
                                <div class="form-group">
                                    <label class="form-label" style="width: 100%;">
                                        Password
                                        <a href="{{ route('password.request') }}" class="float-right small">I forgot password</a>
                                    </label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox"  name="remember"   {{ old('remember') ? 'checked' : '' }} class="custom-control-input" />
                                        <span class="custom-control-label">Remember me</span>
                                    </label>
                                </div>
                                <div class="form-footer">
                                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                </div>
                            </div>
                        </form>
                        <div class="text-center text-muted">
                            Don't have account yet? <a href="{{route('register')}}">Sign up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
