@extends('layouts.backend')

@section('page.title','Wallet')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="form-group">
                    <div class="input-group">
                        Referral Link: <a target="_blank" href="{{route('referral_link',\Auth::user()->id)}}">{{route('referral_link',\Auth::user()->id)}}</a>

                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cards row-deck">
            <div class="col-sm-10 col-lg-10">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Upline</h2>
                        <div class="card-options">
                        </div>
                    </div>

                    <div class="table-responsive">
                    @if(\App\User::is_exit(\Auth::user()->parent_id))
                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">

                            <tbody>

                                <tr>

                                    <td class="text-center">
                                        <div class="avatar d-block" style="background-image: url(demo/faces/female/26.jpg)">
                                        </div>
                                    </td>
                                    <td>
                                        <div>{{\Auth::user()->parent->name}}</div>

                                        <div class="small text-muted">
                                            Registered: {{\Auth::user()->parent->created_at}}
                                        </div>
                                    </td>
                                    <td>

                                        <div>{{\Auth::user()->email}}</div>

                                        <div class="small text-muted">
                                            Balance: {{\Auth::user()->balance()}}
                                        </div>

                                    </td>


                                </tr>

                            </tbody>
                        </table>
                        @endif

                    </div>
                </div>
            </div>
            <div class="col-2 col-sm-2 col-lg-2">
                <div class="card">
                    <div class="card-body p-3 text-center">

                        <div class="h1 m-0">{{\App\Referral::where('parent_id',\Auth::user()->id)->count()}}</div>
                        <div class="text-muted mb-4">Total Referrals</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-10 col-lg-10">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Refferals</h2>
                        <div class="card-options">
                          </div>
                    </div>

                    <div class="table-responsive">

                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">

                            <tbody>
                            @foreach($referrals as $referral)
                                <tr>

                                    <td class="text-center">
                                        <div class="avatar d-block" style="background-image: url(demo/faces/female/26.jpg)">
                                        </div>
                                    </td>
                                    <td>
                                        <div>{{$referral->user->name}}</div>

                                        <div class="small text-muted">
                                            Registered: {{$referral->user->created_at}}
                                        </div>
                                    </td>
                                    <td>
                                        <div>{{$referral->user->email}}</div>

                                        <div class="small text-muted">
                                            Balance: {{$referral->user->balance()}}
                                        </div>

                                    </td>
                                    <td class="text-center">
    Level  {{$referral->level}}
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $referrals->links() !!}
                </div>
            </div>
            <div class="col-sm-2 col-lg-2">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Level Wise</h4>
                    </div>
                    <table class="table card-table">
                        <tbody>

                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_1"> Level 1</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_2"> Level 2</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_3"> Level 3</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_4"> Level 4</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_5"> Level 5</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_6"> Level 6</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_7"> Level 7</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_8"> Level 8</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_9"> Level 9</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_10"> Level 10</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_11"> Level 11</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_12"> Level 12</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_13"> Level 13</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_14"> Level 14</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_15"> Level 15</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_16"> Level 16</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_17"> Level 17</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_18"> Level 18</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_19"> Level 19</a></td>
                        </tr>
                        <tr>
                            <td ><a class="btn btn-xs btn-pill" data-toggle="modal" data-target="#level_20"> Level 20</a></td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
        </div>
    </div>



    @for($i =1;$i<=20;$i++)
        <!-- Modal -->
        <div id="level_{{$i}}" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Level {{$i}} Referrals</h4>

                        <button type="button" class="close" data-dismiss="modal"></button>
                    </div>
                    <div class="modal-body" id="level_{{$i}}_content">
                        <p><iframe src="{{route('auth.referral.table',$i)}}" style="width:100%;height:400px;padding: 0px;margin: 0px; " frameBorder="0"></iframe></p>
                    </div>

                </div>

            </div>
        </div>

    @endfor
@endsection
