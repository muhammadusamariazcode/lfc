@extends('layouts.backend')

@section('page.title','Wallet')
@section('content')


    <div class="container">
        <div class="row row-cards row-deck">
            <div class="col-sm-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">Banners</h2>
                        <div class="card-options">
                        </div>
                    </div>



                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="{{route('referral_link',\Auth::user()->id)}}" readonly="">
                                        <span class="input-group-append">

                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-11 col-sm-offset-1">
                                <a href="{{route('referral_link',\Auth::user()->id)}}"><img src="{{url('banner4.jpg')}}" alt=""></a>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value=' <a href="{{route('referral_link',\Auth::user()->id)}}"><img src="{{url('banner4.jpg')}}" alt=""></a>' readonly="">
                                        <span class="input-group-append">

                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 col-sm-offset-1">
                                <a href="{{route('referral_link',\Auth::user()->id)}}"><img src="{{url('banner1.jpg')}}" alt=""></a>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value=' <a href="{{route('referral_link',\Auth::user()->id)}}"><img src="{{url('banner1.jpg')}}" alt=""></a>' readonly="">
                                        <span class="input-group-append">

                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1" style="margin: 50px">
                                <a href="{{route('referral_link',\Auth::user()->id)}}"><img src="{{url('banner2.jpg')}}" alt=""></a>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value=' <a href="{{route('referral_link',\Auth::user()->id)}}"><img src="{{url('banner2.jpg')}}" alt=""></a>' readonly="">
                                        <span class="input-group-append">

                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-sm-offset-1">
                                <a href="{{route('referral_link',\Auth::user()->id)}}"><img src="{{url('banner3.jpg')}}" alt=""></a>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value=' <a href="{{route('referral_link',\Auth::user()->id)}}"><img src="{{url('banner3.jpg')}}" alt=""></a>' readonly="">
                                        <span class="input-group-append">

                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                </div>
            </div>

        </div>
    </div>



@endsection
