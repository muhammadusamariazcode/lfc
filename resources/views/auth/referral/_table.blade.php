<html lang="en" dir="ltr">
<head>
    <meta name="csrf-token" content="yA4rPTRgXcmudeqNS6j9qMMah4biRySJb2Ap9XOc">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="http://localhost/vendor/tabler/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="http://localhost/vendor/tabler/favicon.ico" />
    <!-- Generated: 2018-04-06 16:33:39 +0200 -->
    <title>{{env('APP_NAME')}} - Wallet</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <script src="http://localhost/assets/js/require.min.js"></script>
    <script>
        requirejs.config({
            baseUrl: '.'
        });
    </script>
    <!-- Dashboard Core -->
    <link href="http://localhost/assets/css/dashboard.css" rel="stylesheet" />
    <script src="http://localhost/assets/js/dashboard.js"></script>
    <!-- c3.js Charts Plugin -->
    <link href="http://localhost/assets/plugins/charts-c3/plugin.css" rel="stylesheet" />
    <script src="http://localhost/assets/plugins/charts-c3/plugin.js"></script>
    <!-- Google Maps Plugin -->
    <link href="http://localhost/assets/plugins/maps-google/plugin.css" rel="stylesheet" />
    <script src="http://localhost/assets/plugins/maps-google/plugin.js"></script>
    <!-- Input Mask Plugin -->
    <script src="http://localhost/assets/plugins/input-mask/plugin.js"></script>

    <script src="http://localhost/assets/plugins/input-mask/plugin.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="http://localhost/assets/plugins/jquery-qrcode/jquery.qrcode.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.js"></script>


</head>
<body class="">
@if(count($referrals) == 0)

    <div class="alert alert-primary">You dont have any referrals in this level</div>
@else

<div class="table-responsive">

    <table class="table table-hover table-outline table-vcenter text-nowrap card-table">

        <tbody>
        @foreach($referrals as $referral)
            <tr>


                <td>
                    <div>{{$referral->user->name}}</div>

                    <div class="small text-muted">
                        Registered: {{$referral->user->created_at}}
                    </div>
                </td>
                <td>
                    <div>{{$referral->user->email}}</div>

                    <div class="small text-muted">
                        Balance: {{$referral->user->balance()}}
                    </div>

                </td>
                <td class="text-center">
                    Level  {{$referral->level}}
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>
{!! $referrals->links() !!}
@endif
</body>
