<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{env('APP_NAME')}}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="/assets/frontend/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/frontend/css/style.css">
</head>
<body id="home">

<!--Top section-->

<!-- SHOWCASE -->
<section id="showcase">
    <div class="primary-overlay text-white">
        <div class="container">
            <div class="row pt-1">
                <div class="col-sm-12 m-auto">
                    <nav class="navbar navbar-expand-md navbar-light bg-light rounded">
                        <div class="container">
                            <a href="index.html" class="navbar-brand">
                                <img src="/assets/frontend/img/logo_.png" alt="" class="w-100">
                            </a>
                            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav"><span class="navbar-toggler-icon"></span></button>
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a href="#home" class="nav-link link-text mt-2 mx-3">WALLET</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#about" class="nav-link link-text mt-2 mx-3">BUY/SELL</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#about" class="nav-link link-text mt-2 mx-3">ADVANTAGES</a>
                                    </li>
                                    <li class="nav-item btn btn-outline-warning rounded mx-3 ">
                                        <a href="{{route('register')}}" class="nav-link px-3 link-text">Sign Up</a>
                                    </li>
                                    <li class="nav-item btn btn-outline-warning rounded mx-3 active">
                                        <a href="{{route('login')}}" class="nav-link px-4 link-text">Login</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-6 text-left margin-t">
                    <h1 class="display-2 mt-5 pt-5 txt-bold">
                        LIVE FUTURE COIN.
                    </h1>
                    <p class="lead p-text-set">A Perfect Mobile App For Live Future Coin</p>
                    <p class="lead">
                        Live future coin may have introduced the solution which could help us eliminate issue that plagues the entire world. Its helps to those people which are worried about our career and they cannot find any job, they can invest in our live Future coins and earn on daily basis.
                        <br />   <br />
                        Live future coin provides a medium for its customers through whom they can make money without long term commitment and very low chance of loss.  It is a way of money transfer which only involves the buyers and the sellers or in other words, investors and exchangers. The website does not get involved so it’s a direct and accessible way of transferring money and making money without any hassle.
                        <br />   <br />
                        What live future coin aims to do is to provide a platform where the sellers shall invest their coins to the exchangers and the exchangers will themselves sell the coins further.

                    </p>
                    <a href="#" class="btn btn-outline-secondary btn-lg text-white">
                        <i class="fa fa-arrow-right pr-3"></i>GET APP</a>
                </div>
                <div class="col-lg-6">
                    <img src="/assets/frontend/img/mobile.png" alt="" class="img-fluid d-none d-lg-block">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- NEWSLETTER -->
<!--   <section id="newsletter" class="bg-dark text-white py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <input type="text" class="form-control form-control-lg" placeholder="Enter Name">
        </div>
        <div class="col-md-4">
          <input type="text" class="form-control form-control-lg" placeholder="Enter Email">
        </div>
        <div class="col-md-4">
          <button class="btn btn-primary btn-lg btn-block"><i class="fa fa-envelope-open-o"></i> Subscribe</button>
        </div>
      </div>
    </div>
  </section> -->

<!-- BOXES -->
<section id="boxes" class="py-5">
    <div class="container-fluid">
        <div class="row">
            <!--  <div class="col-md-3">
               <div class="text-left border-primary">
                 <div class="card-body card-back-LiveFutureCoin ">
                   <h5>TRENDING HOT</h5>
                   <h2 class="text-primary ">Get Started With Bitcoin</h2>
                   <p class="text-muted card-txt-set lead">Become well informed about the things you need to know to use Bitcoins securely to avoid any pitfalls.</p>
                   <div class="btn btn-warning btn-lg text-light"> GET STARTED</div>
                 </div>
               </div>
             </div> -->
            <div class="col-md-2 ml-auto">
                <div class="card text-center">
                    <div class="card-body">
                        <img src="/assets/frontend/img/get_a_bitcoin_wallet.png" alt="" class="py-1">
                        <h3 class="py-1 font-weight-bold text-secondary">Get a LFC wallet</h3>
                        <p class="py-1 text-secondary lead">Customers can register themeselfs and make there own account in LFC and get a LFC wallet.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card text-center">
                    <div class="card-body">
                        <img src="/assets/frontend/img/bit.png" alt="" class="py-1">
                        <h3 class="py-1 font-weight-bold text-secondary">How to get LFC?</h3>
                        <p class="py-1 text-secondary lead">Customers get LFC for deposit any type of transaction using LFC. Customer can withdraw money through LFC</p>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card text-center ">
                    <div class="card-body">
                        <img src="/assets/frontend/img/use-bitcoin.png" alt="" class="py-1">
                        <h3 class="py-1 font-weight-bold text-secondary">How to Use LFC?</h3>
                        <p class="py-1 text-secondary lead">Customers can deposit and withdraw money with given LFC method.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 mr-auto">
                <div class="card text-center ">
                    <div class="card-body">
                        <img src="/assets/frontend/img/bitcoin_news.png" alt="" class="py-1">
                        <h3 class="py-1 font-weight-bold text-secondary">LiveFutureCoin News</h3>
                        <p class="py-1 text-secondary lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, quibusdam.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ABOUT / WHY SECTION -->
<section id="about" class="py-5 text-center">
    <div class="container px-5">
        <div class="row">
            <div class="col-sm-12">
                <div class="info-header mb-5">

                    <h3 class="text-muted">ITS EASY & SECURE</h3>
                    <h1 class="text-primary pb-3">
                        How It Works?
                    </h1>
                    <p class="lead pb-3">
                       LFC is almost always a good investment, as long as you invest wisely. As the value of currency weakens, the price of LFC trends to go up. Research the current price of LFC before buying and selling any LFC
                    </p>
                </div>
            </div>
            <!-- ACCORDION -->
            <div class="row  text-center">
                <div class="col-md-3 col-sm-12 ml-auto">
                    <div class="card">
                        <div class="card-header" id="heading1">
                            <img src="/assets/frontend/img/register_account.png" alt="">
                            <h4 class="mb-0 font-weight-bold text-dark py-2">
                                Create your Wallet
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="lead"> Making a cool LFC wallet is easy. Just get yourself some scrap strips of LFC ant you are good to go. No special tool required. </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="card">
                        <div class="card-header" id="heading1">
                            <img src="/assets/frontend/img/register_account.png" alt="">
                            <h4 class="mb-0 font-weight-bold text-dark py-2">
                                Buy LFC
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="lead"> Firstly signup for a exchange account. This will give u a secure place to store your LFC . After it we will complete your buy and deliver your coin. The price of coin is change as a passage of time. We will show you the coin price before you buy</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 mr-auto">
                    <div class="card">
                        <div class="card-header" id="heading1">
                            <img src="/assets/frontend/img/register_account.png" alt="">
                            <h4 class="mb-0 font-weight-bold text-dark py-2">
                                Receiving commission
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="lead">if u buy a coin your commission is send to your wallet. You can collect it. 1% commission daily on having 1000lfc and up to 40% commission on referrals earning.
                        </p>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<!-- AUTHORS -->
<section id="authors" class="my-5 text-center bg-light">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="info-header mb-5">
                    <h4 class="text-muted mt-5">ALL THESE SERVICES FOR YOU</h4>

                    <h1 class="text-primary pb-3 display-3">
                        The Best Services
                    </h1>
                    <p class="lead pb-3">
                        What Live future coin aims to do is to provide a platform where the sellers shall invest their coins to the exchangers and the exchangers will themselves sell the coins further.
                    </p>
                </div>
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-lg-4 col-md-6">
                <div class="card   ">
                    <div class="card-body bg-light">
                        <img src="/assets/frontend/img/digital_money.png" alt="" class="img-fluid">
                        <h3 class="mt-5 font-weight-bold text-dark">Digital Money</h3>
                        <p  class="text-dark lead py-5">A digital currency is a form of currency that is available only in digital or electronic form, and not in physical form. It is also called digital money, electronic money, electronic currency, or cyber cash.</p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card  active">
                    <div class="card-body">
                        <img src="/assets/frontend/img/bitcoin_wallet.png" alt="" class="img-fluid">
                        <h3 class="mt-5 font-weight-bold text-light">Crypto currency</h3>

                        <p class="text-light lead py-5">A crypto currency is a digital or virtual currency designed to work as a medium of exchange. It uses cryptography to secure and verify transactions as well as to control the creation of new units of a particular crypto currency.</p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card bg-light  px-5 ">
                    <div class="card-body">
                        <img src="/assets/frontend/img/bitcoin_mining.png" alt="" class="img-fluid">
                        <h3 class="mt-5 font-weight-bold text-dark">Secure Transaction</h3>

                        <p  class="text-dark lead py-5">Secure Electronic Transaction (SET) is a system for ensuring the security of financial transactions on the Internet.</p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card bg-light px-5">
                    <div class="card-body">
                        <img src="/assets/frontend/img/crypto_currency.png" alt="" class="img-fluid">
                        <h3 class="mt-5 font-weight-bold text-dark">Our Marchant</h3>

                        <p  class="text-dark lead py-5">we are providing recently two website through LFC. <br />i)lfcshooping.com   <br />ii)lfcdealmarts.com.  <br />we are providing more site through LFC in comming soon.</p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card bg-light px-5">
                    <div class="card-body">
                        <img src="/assets/frontend/img/bitcoin_mining.png" alt="" class="img-fluid">
                        <h3 class="mt-5 font-weight-bold text-dark">Coin Mining</h3>

                        <p  class="text-dark lead py-5">The mining process involves compiling recent transactions into blocks and trying to solve a computationally difficult puzzle.</p>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card bg-light">
                    <div class="card-body">
                        <img src="/assets/frontend/img/mining_technology.png" alt="" class="img-fluid">
                        <h3 class="mt-5 font-weight-bold text-dark">Mining Technology</h3>

                        <p class="text-dark lead py-5">Mining, in the context of block chain technology, is the process of adding transactions to the large distributed public ledger of existing transactions, known as the block chain.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 py-5">
                <h4 class="text-muted text-sm-center" style="width: 100%;">WANNA LEARN NOW?</h4>
                <h1 class="font-weight-bold text-dark display-2 text-sm-center">Learn How live future coin Works From This Video</h1>
                <p class="lead text-sm-center">You don’t have to reinvent the wheels, right? When you
                    entrust the task of launching a strategy for your latest
                    marketing campaign, you would get many time tested
                    modules that have delivered consistently across.</p>
            </div>
            <div class="col-md-6 col-sm-12 learn-img">
                <img src="/assets/frontend/img/learn_how_bitcoin_image.png" class="img-fluid py-5" style="width: 100%" alt="">
            </div>
        </div>
    </div>
</section>
<footer id="foot" class="bg-dark">
    <div class="container">
        <div class="row ">
            <div class="col-md-2 col-sm-4 col-xs-2 ml-auto">
                <ul class="list-group">
                    <li class="list-unstyled py-4">
                        <img src="/assets/frontend/img/footer_log.png" class="img-fluid" alt="">
                    </li>

                    <li class="list-unstyled py-2">
                        <span><i class="far fa-envelope top-ico p-2 top-ico px-2"></i></span><h5 class="d-inline text-light">livefuturecoin@gmail.com</h5>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-2">
                <ul class="list-group">
                    <li class="list-unstyled py-4">
                        <h3 class="text-light font-weight-bold py-4 bor-bot">Products</h3><hr>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Buy And Sell Digital Currency</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">GDAX</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Life Future Coin Commerce</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Developer PlatForm</h5>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-2">
                <ul class="list-group">
                    <li class="list-unstyled py-4">
                        <h3 class="text-light font-weight-bold py-4 bor-bot">Learn</h3><hr>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Buy Bitcoin</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Buylive future coinCash</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Buy Ethereum</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Buy Litecoin</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Supported countries</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Status</h5>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-2">
                <ul class="list-group">
                    <li class="list-unstyled py-4">
                        <h3 class="text-light font-weight-bold py-4 bor-bot">Company</h3><hr>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Home</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">About Us</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Services</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Our Shop</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Latest Blog</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Contatc Us</h5>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-4 mr-auto">
                <ul class="list-group">
                    <li class="list-unstyled py-4">
                        <h3 class="text-light font-weight-bold py-4 bor-bot">Socila Media</h3><hr>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Blog</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Twitter</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Facebook</h5>
                    </li>
                    <li class="list-unstyled py-3">
                        <span><i class="fas fa-angle-right text-warning px-2"></i></span><h5 class="d-inline text-light">Google+</h5>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="/assets/frontend/js/jquery.min.js"></script>
<script src="/assets/frontend/js/popper.min.js"></script>
<script src="/assets/frontend/js/bootstrap.min.js"></script>
<script src="/assets/frontend/js/navbar-fixed.js"></script>
</body>
</html>
