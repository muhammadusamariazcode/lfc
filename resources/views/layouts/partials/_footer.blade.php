<div id="sendModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Send {{config('app.symbol','')}}</h4>
                <span class="pull-right" data-dismiss="modal"><i class="fa fa-close"></i> </span>
            </div>
            <div class="modal-body">
                <p>
                <form id="send_form" action="{{route('auth.wallet.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="address">Address:</label>
                        <input type="text" class="form-control" id="address" name="address" required>
                    </div>
                    <div class="form-group">
                        <label for="amount">Amount:</label>
                        <input type="text" class="form-control" id="amount" name="amount" required>
                    </div>
                    <!-- <div class="form-group">
                         <label for="fees">Fees:</label>
                         <input type="text" class="form-control" id="fees" readonly>
                     </div>
                     <div class="form-group">
                         <label for="fees">Balance:</label>
                         <input type="text" class="form-control" id="deduct" readonly>
                     </div>
                     <div class="form-group">
                         <label for="fees">Total Amount will deduct:</label>
                         <input type="text" class="form-control" id="deduct" readonly>
                     </div>
                     <div class="form-group">
                         <label for="fees">Balance After Transaction:</label>
                         <input type="text" class="form-control" id="deduct" readonly>
                     </div>-->
                    <button type="submit"

                            onClick="event.preventDefault();this.disabled=true; this.innerHTML='Processing...'; document.getElementById('send_form').submit();"
                            class="btn btn-default">Submit</button>
                </form>
                </p>
            </div>

        </div>

    </div>
</div>
<div id="receiveModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Receive  {{config('app.symbol','')}}</h4>
                <span class="pull-right" data-dismiss="modal"><i class="fa fa-close"></i> </span>
            </div>
            <div class="modal-body">
                <p>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" value="{{\Auth::user()->default_address()->address}}" readonly>
                        <span class="input-group-append">
                                    <button class="btn btn-primary" type="button"><i class="fe fe-copy"></i></button>
                            </span>
                    </div>
                </div>
                </p>
            </div>

        </div>

    </div>
</div>

<!-- Essential javascripts for application to work-->
<script src="/assets/backend/js/jquery-3.2.1.min.js"></script>
<script src="/assets/backend/js/popper.min.js"></script>
<script src="/assets/backend/js/bootstrap.min.js"></script>
<script src="/assets/backend/js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="/assets/backend/js/plugins/pace.min.js"></script>
<!-- Page specific javascripts-->
<script type="text/javascript" src="/assets/backend/js/plugins/chart.js"></script>
<script type="text/javascript">
    var data = {
        labels: ["January", "February", "March", "April", "May"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56]
            },
            {
                label: "My Second dataset",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [28, 48, 40, 19, 86]
            }
        ]
    };
    var pdata = [
        {
            value: 300,
            color: "#46BFBD",
            highlight: "#5AD3D1",
            label: "Complete"
        },
        {
            value: 50,
            color:"#F7464A",
            highlight: "#FF5A5E",
            label: "In-Progress"
        }
    ]

    var ctxl = $("#lineChartDemo").get(0).getContext("2d");
    var lineChart = new Chart(ctxl).Line(data);

    var ctxp = $("#pieChartDemo").get(0).getContext("2d");
    var pieChart = new Chart(ctxp).Pie(pdata);
</script>
<!-- Google analytics script-->
<script type="text/javascript">
    if(document.location.hostname == 'pratikborsadiya.in') {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-72504830-1', 'auto');
        ga('send', 'pageview');
    }
</script>


@yield('scripts')
</body>
</html>