<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/backend/css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <title>@yield('page.title')</title>
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="app sidebar-mini rtl">
<!-- Navbar-->
<header class="app-head">
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <button class="navbar-toggler left-mar" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <!--   <li class="nav-item padd active">
              <a href=""> <span><img src="/assets/backend/img/box.png" alt="" class="px-2"></span></a>
            </li> -->
                <!--      <li class="nav-item padd ">
               <span class="mr-2 px-2"><a href="" class="">Mega</a></span>
            </li> -->
                <!--  <li class="nav-item padd ">
              <span class="mr-2  px-2"><img src="/assets/backend/img/serch.png" alt=""></span>
            </li> -->
                <li class="nav-item padd padd-l">
                    <span class="mr-2"><a href="#" class="bt-set" data-toggle="modal" data-target="#sendModal">Send</a></span>
                </li>
                <li class="nav-item padd ">
                    <span class="mr-2 "><a href="#" class="bt-set" data-toggle="modal" data-target="#receiveModal">Receive</a></span>
                </li>
                <li class="nav-item padd ">
                    <span class="mr-2"><a href="" class="bt-set">Balance:{{\Auth::user()->balance()}} {{config('app.symbol')}}</a></span>
                </li>
                <li class="nav-item padd ">
                    <span class="mr-2 "><a href="" class="bt-set">1 {{config('app.symbol')}} = $0.07</a></span>
                </li>
            </ul>
            <ul class="navbar-nav sm-inline">
                <li class="nav-item dropdown d-sm"><a class="app-nav__item text-dark mt-3 font-weight-bold" >Hello, {{\Auth::user()->name}}</a>
                </li>

            </ul>
        </div>
    </nav>
    <a class="app-sidebar__toggle" href="#" data-toggle="sidebar"></a>
</header>