<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="m-auto py-3 img-fluid" src="/assets/backend/img/log.png" alt="User Image">
    </div>
    <ul class="app-menu">
        <li><a class="app-menu__item active text-center" href="{{route('home')}}"><img src="/assets/backend/img/dashboard.png" alt=""><span class="app-menu__label d-inline">Dashboard</span></a></li>
        <li class="treeview"><a class="app-menu__item text-center" href="{{route('auth.wallet.index')}}"><img src="/assets/backend/img/transaction.png" alt=""><span class="app-menu__label d-inline">Transaction History</span></a>
        </li>
        <li class="treeview"><a class="app-menu__item text-center" href="{{route('auth.referral.index')}}" ><img src="/assets/backend/img/referrals.png" alt="" class="p-2"><span class="app-menu__label d-inline">Referrals</span></a></li>

        <li class="treeview"><a class="app-menu__item text-center" href="{{route('auth.referral.banner.index')}}"><img src="/assets/backend/img/banners.png" alt=""class="p-2"><span class="app-menu__label d-inline">Banners</span></a></li>

        <li class="treeview"><a class="app-menu__item text-center" href="{{route('auth.profile.index')}}"><img src="/assets/backend/img/settings.png" alt="" class="p-2"><span class="app-menu__label d-inline">Settings</span></a></li>



        <li class="treeview">

            <a class="app-menu__item text-center" href="http://explorer.livefuturecoin.com"><img src="/assets/backend/img/blockchain.png" alt="" class="p-2"><span class="app-menu__label">BlockChain Explorer</span></a>
            <a class="app-menu__item text-center" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                 <img src="/assets/backend/img/logout.png" alt="" class="p-3"><span class="app-menu__label">Logout</span>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>

    </ul>
</aside>