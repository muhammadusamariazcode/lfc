

<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
    <meta name="description" content="">
    <meta name="theme-color" content="#7155aa">

    <link rel="shortcut icon" href="{{url('favicon.ico')}}" />

    <!-- Fonts -->
    <link href="{{url('css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Bootstrap core CSS -->

    <!-- Custom styles for this template -->
    <link href="{{url('frontend/css/chosen.css')}}" rel="stylesheet" />
    <link href="{{url('frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{url('frontend/css/owl.carousel.css')}}" rel="stylesheet"  />

    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
    <title>BITFIL COIN</title>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />


    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <!--[if lt IE 9]>
    <link rel='stylesheet' id='twentyfifteen-ie-css'  href='https://edinarcoin.com/wp-content/themes/twentyfifteen/css/ie.css?ver=20141010' type='text/css' media='all' />
    <![endif]-->
    <!--[if lt IE 8]>
    <link rel='stylesheet' id='twentyfifteen-ie7-css'  href='https://edinarcoin.com/wp-content/themes/twentyfifteen/css/ie7.css?ver=20141010' type='text/css' media='all' />
    <![endif]-->


    <meta name="generator" content="WordPress 4.9.4" />
    <link rel="canonical" href="./" />
    <link rel='shortlink' href='./' />
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed322d.json?url=https%3A%2F%2Fedinarcoin.com%2F" />
    <link rel="alternate" href="./" hreflang="en" />
    <link rel="alternate" href="#" hreflang="ru" />
    <link rel="alternate" href="#" hreflang="zh" />
    <link rel="alternate" href="#" hreflang="th" />
    <link rel="alternate" href="#" hreflang="hi" />
    <link rel="alternate" href="#" hreflang="ar" />
    <link rel="alternate" href="#" hreflang="id" />
    <link rel="alternate" href="#" hreflang="tr" />
    <link rel="alternate" href="#" hreflang="ja" />
    <link rel="alternate" href="#" hreflang="ko" />
    <link rel="alternate" href="#" hreflang="bg" />
    <link rel="alternate" href="#" hreflang="hu" />
    <link rel="alternate" href="#" hreflang="pt" />
    <link rel="alternate" href="#" hreflang="kk" />
    <link rel="alternate" href="#" hreflang="it" />
    <link rel="alternate" href="#" hreflang="de" />
    <link rel="alternate" href="#" hreflang="el" />
    <link rel="alternate" href="#" hreflang="es" />
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>

</head>

<body class="home page-template-default page page-id-81">
<!-- header -->
<header id='header' class="header header-default navbar-fixed-top brand-fixed brand-center">
    <input type="hidden" value="https://edinarcoin.com/" class="current_url" />
    <input type="hidden" value="" class="current_lang_slug" />
    <div class="container container-sm">
        <nav class="nav-row">
            <a href="{{\URL::to('/')}}" class="navbar-brand">
                <img src="{{url('frontend/logo.png')}}" alt="">
            </a>
            <i class="fa fa-bars nav-btn"></i>
            <div class="collapse-mobile-nav">
                <ul class="list-inline pull-left text-uppercase">
                    <li class="active">
                        <a class="list" href="{{\URL::to('/')}}">wallets</a>
                    </li>
                    <li class="">
                        <a class="list" href="{{route('frontend.buysell')}}">Buy/Sell</a>
                    </li>
                    <li class="">
                        <a class="list" href="{{route('frontend.advantage')}}">advantages</a>
                    </li>

                </ul>
                <ul class="list-inline pull-right text-uppercase">
                    <li>
							<span class='list list-division'>{{env('COIN_SYMBOL')}} $0.03
                                <!--<i class="fa fa-long-arrow-up text-success secondary"></i>-->
							</span>
                    </li>


                    <li class="">
                        <a class='list' href="{{route('login')}}">Login</a>
                    </li>
                    <li class="">
                        <a class='list' href="{{route('register')}}">Register</a>
                    </li>

                </ul>
            </div>
        </nav>
    </div>
</header>







<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        @yield('content')


    </main><!-- .site-main -->
</div><!-- .content-area -->



<!-- footer -->
<footer class="footer">

    <div class="container container-sm">
        <div class="row division-list">

            <div class="col-xs-12 col-sm-4 col-md-5 col-cell">
                <div class="title-wrp">
                    <label class="h1 title text-uppercase">Contact us</label>
                </div>


                <ul class="list-group list-group-clear">
                    <li class="list-group-item">
                        admin@{{env('APP_NAME')}}.com					</li>
                    <li class="list-group-item">
                        support@{{env('APP_NAME')}}.com
                    </li>
                </ul>
            </div><!-- col-xs-12 col-sm-4 -->

            <div class="col-xs-12 col-sm-4 col-cell">
                <div class="title-wrp">
                    <label class="h1 title text-uppercase">wallets					</label>
                </div>
                <ul class="list-group list-group-clear text-uppercase">
                    <li class="list-group-item">
                        <a href="#" data-event="downloadAndroid" class="wallets_metrika">
                            <i class="fa fa-android fa-primary fa-inverse fa-2lg"></i>Android Wallet						</a>
                    </li>
                    <li class="list-group-item">
                        <a target="_blank" href="#" data-event="webWallet" class="wallets_metrika">
                            <i class="fa fa-android fa-blockchain fa-primary fa-inverse fa-2lg"></i>Web Wallet						</a>
                    </li>

                </ul>
            </div><!-- col-xs-12 col-sm-4 -->

            <div class="col-xs-12 col-sm-4 col-md-3 col-cell">

                <ul class="list-group list-group-clear text-uppercase">
                    <li class="list-group-item">
                        <a href="#">Cooperation</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#">User Guide</a>
                    </li>
                    <li class="list-group-item">
                        <a href="#">FAQ</a>
                    </li>


                    <li class="list-group-item">
                        <a href="#">Contact</a>
                    </li>

                </ul>
            </div><!-- col-xs-12 col-sm-4 -->

            <div class="col-sm-12 footer__soc">
                <ul class="list-inline">
                    <li>
                        <a href="#" target="_blank">
                            <i class="fa fa-facebook fa-primary fa-inverse fa-2lg"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="fa fa-twitter fa-primary fa-inverse fa-2lg"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="fa fa-instagram fa-primary fa-inverse fa-2lg"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            <i class="icon-mmgp"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div><!-- division-list -->
        <div class="text-center footer-bottom-wrp">
            <p class="text-upppercase">&copy; BLOCKCHAIN SECURE CRYPTOCYRRENCY LTD.</p>
            <p>{{env('APP_NAME')}}.com</p>
        </div>
    </div>

</footer>
<!-- END | footer -->


<!-- Bootstrap core JavaScript
================================================== -->
<script src="{{url('frontend/js/jquery.min.js')}}"></script>
<script src="{{url('frontend/js/bootstrap.js')}}"></script>
<script src="{{url('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{url('frontend/js/chosen.jquery.js')}}"></script>
<script src="{{url('frontend/js/ed.js')}}"></script>


</body>

</html>