@if(!\Auth::guest())
    @if(\Request::route()->getName() != 'auth.google2fa.index')
        @if(!\Auth::user()->is_google2fa)


            <div class="row">
    <div class="col-md-12" style="margin-bottom: 10px;">
        <div class="py-1" style="background-color: #d2d5da;">
            <img src="/assets/backend/img/securty_alrt.png" alt="" class="p-3">
            <span class="d-inline"><strong>Security Alert!</strong>  Please Enable Google 2fa  Security  From <a href="{{route('auth.google2fa.index')}}">here</a></span>
        </div>
    </div>
</div>
@endif
        @endif
    @endif