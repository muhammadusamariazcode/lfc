    <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
    Route::get('{user_id}/referral_link',
        function ($user_id){
           if( \App\User::is_exit($user_id)){
               session(['r'=>$user_id]);
              if(!\Auth::guest()){
                  \Auth::logout();

              }
           }

           return redirect()->route('register');

        })->name('referral_link');


  Route::get('buysell',function(){
     //   return view('frontend.buysell');
  })->name('frontend.buysell');

    Route::get('advantage',function(){
        return view('frontend.advantage');
    })->name('frontend.advantage');

    Route::get('media',function(){
        return view('frontend.media');
    })->name('frontend.media');