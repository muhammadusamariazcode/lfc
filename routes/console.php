<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('stake', function () {
    (new  \App\Http\Controllers\Cron\StakeController())->process();

    (new  \App\Http\Controllers\Cron\StakeController())->paid();

    (new  \App\Http\Controllers\Cron\StakeController())->generate();

    (new  \App\Http\Controllers\Cron\CommissionController())->paid();

})->describe('Display an inspiring quote');

Artisan::command('commission', function () {
    (new  \App\Http\Controllers\Cron\CommissionController())->paid();




})->describe('Display an inspiring quote');


Artisan::command('corect_referral_table_job', function () {
    \App\Http\Controllers\Auth\RegisterController::corect_referral_table_job();




})->describe('Display an inspiring quote');


Artisan::command('sync', function () {
    while(true){
        try{

            \App\Account::chunk(100,function($accounts){
                foreach($accounts as $account){
                    try{

                        $account->sync();
                    }catch (\Exception $ex){
                        echo $ex->getMessage();
                        echo "\n";
                    }
                }
            });
        }catch (\Exception $ex){
            echo $ex->getMessage();  echo "\n";
        }
        sleep(50);

    }



})->describe('Display an inspiring quote');
Artisan::command('sync_balance', function () {
    while(true){
        try{

            \App\Account::chunk(100,function($accounts){
                foreach($accounts as $account){
                    try{
                        $account->sync_balance();
                    }catch (\Exception $ex){
                        echo $ex->getMessage();
                        echo "\n";
                    }
                }
            });
        }catch (\Exception $ex){
            echo $ex->getMessage();  echo "\n";
        }

        sleep(1);
    }



})->describe('Display an inspiring quote');
