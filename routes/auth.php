    <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('status','HomeController@status')->name('status');

Route::group(['prefix'=>'profile'],function (){
    Route::get('/','Auth\Profile\ProfileController@index')->name('auth.profile.index');
    Route::post('update','Auth\Profile\ProfileController@update')->name('auth.profile.update');
    Route::post('update_password','Auth\Profile\ProfileController@update_password')->name('auth.profile.update_password');

});


    Route::group(['prefix'=>'google2fa'],function (){
        Route::get('/','Auth\Security\Google2faController@index')->name('auth.google2fa.index');
        Route::post('toggle','Auth\Security\Google2faController@toggle')->name('auth.google2fa.toggle');


        Route::get('ask','Auth\Security\Google2faController@ask')->name('auth.google2fa.ask');
        Route::post('check','Auth\Security\Google2faController@check')->name('auth.google2fa.check');


    });


    Route::group(['prefix'=>'transactions'],function (){
        Route::get('/','Auth\Wallet\WalletController@index')->name('auth.wallet.index');
        Route::get('staking','Auth\Wallet\StakeController@index')->name('auth.wallet.staking');
        Route::get('commission','Auth\Wallet\CommissionController@index')->name('auth.wallet.commission');
        Route::get('earning','Auth\Wallet\EarningController@index')->name('auth.wallet.earning');

        Route::post('store','Auth\Wallet\WalletController@store')->name('auth.wallet.store');

    });
    Route::group(['prefix'=>'referral'],function (){
        Route::get('/','Auth\Referral\ReferralController@index')->name('auth.referral.index');
        Route::get('{level}/table','Auth\Referral\ReferralController@table')->name('auth.referral.table');

        Route::group(['prefix'=>'banner'],function (){
            Route::get('/','Auth\Referral\BannerController@index')->name('auth.referral.banner.index');

        });


    });



    Route::group(['prefix'=>'blockchain'],function (){
        Route::get('/','Auth\Profile\ProfileController@index')->name('auth.blockchain.index');

    });

    Route::group(['prefix'=>'exchange'],function (){
        Route::get('/','Auth\Profile\ProfileController@index')->name('auth.exchange.index');

    });

