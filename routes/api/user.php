<?php
/**
 * Created by IntelliJ IDEA.
 * User: muham
 * Date: 24/05/2018
 * Time: 9:32 AM
 */


Route::post('login','Api\UserController@login');
Route::post('register','Api\UserController@register');


Route::post('balance','Api\UserController@balance');

Route::post('transactions','Api\UserController@transactions');

Route::post('send','Api\UserController@send');